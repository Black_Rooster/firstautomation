﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace FirstAutomationTest
{
    [TestFixture]
    public class Test
    {
        IWebDriver _driver;

        [OneTimeSetUp]
        public void Init()
        {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("--start-maximized");
            chromeOptions.AddArgument("--incognito");
            _driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), chromeOptions);
            _driver.Navigate().GoToUrl("https://www.mylexisnexis.co.za/Auth/SignOn.aspx");
        }

        [Test]
        public void GIVEN_ValidLoginCredentials_WHEN_LoggingIn_THEN_RenderdLandingPage()
        {
            By usernameTxtBx = By.Name("Username");
            By passwordTextBx = By.Name("Password");
            By loginButton = By.Name("LoginButton");

            _driver.FindElement(usernameTxtBx).SendKeys("gregc");
            _driver.FindElement(passwordTextBx).SendKeys("gregc");

            _driver.FindElement(loginButton).Click();
        }

        [TearDown]
        public void Dispose()
        {
            _driver.Dispose();
        }
    }
}
